import React, { useEffect, useState } from "react";
import "./defaultStyle.css"

function LodgingList() {
    const [ favorites, setFavorites ] = useState(JSON.parse(localStorage.getItem('favorites')))
    const [ lodgings, setLodgings ] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3000/goods.json')
            .then(response => response.json())
            .then(data => setLodgings(data))
    }, [])

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    const addFavorite = (index) => {
        if (!favorites.findIndex((data) => data.id === index.id)) {
            setFavorites([...favorites, index]);
            if(alert("숙소가 찜 되었습니다. 찜 목록을 보러 가시겠습니까?")) {

            } else return;
        } else alert("숙소 찜이 취소되었습니다.");

    }

    return (
        <div className="lodging-list">
            <header>
                <div>펜션 / 풀빌라</div>
                <button>찜한 숙소 보러 가기</button>
            </header>
            <div className="product-info">
                {lodgings.map(item => {
                    return (
                        <div key={lodgings.id} className="lodging-detail">
                            <img src={process.env.PUBLIC_URL + item.imgUrl} width={400} />
                            <div className="lodging-detail-info">
                                <p>{item.name}</p>
                                <p>{item.price} 원</p>
                            </div>
                            <button onClick={addFavorite}>찜하기</button>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default LodgingList;