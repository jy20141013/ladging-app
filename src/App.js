import Favorite from "./pages/Favorite";
import LodgingList from "./pages/LodgingList";
import React, { useEffect , useState } from "react";

function App() {
  useEffect(() => {
    localStorage.setItem("key", 10)
  }, []);
  return (
    <div className="App">
      <LodgingList />
      <Favorite />
    </div>
  );
}

export default App;
